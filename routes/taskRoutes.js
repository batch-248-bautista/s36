//contain all the endpoints for our application
//separate the routes such that "index.js" only contains information in the server
//we need to use express' router() function to achieve this

const express = require("express");

//routervariabel thatwill hold the xpress router
//creates a router insth=ance that fucntions as a midllewarea and routin system
// make it easier to create routes for our application
const router = express.Router();

//the taskController allows use to use he functions defined in the taskCOntroler.js
const taskController = require ("../controllers/taskController");

//route
//the routes are esponsible for defining the URIs that our cliet accesses and the correspondinf controller function that will beused when a route is accesed.
//all the business logic is done in the conroller

//route to get all the tasks
//this route expects to recieve a GET request a the URL "/tasks"

router.get("/",(req, res)=>{
	// 
	//resultFromCOntrolle.then(resultFromCOntroller)is only used here to make code easier to understand
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

//Mini Activity Start

// Route to create a new task
// This route expects to receive a POST request at the URL "/tasks"
// The whole URL is at "http://localhost:3001/tasks"
router.post("/", (req, res) => {

	// The "createTask" function needs the data from the request body, so we need to supply it to the function
	// If information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property
	//taskController.createTask(req.body);
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
	
})

//Mini-Activity End

//route to delte task
//this route expects to recieve a DELETE request at the URL "/tasks/:id"
//the whole url is at "http://localhost:4000/tasks/:id"
// :id wild card 
router.delete("/:id",(req, res)=>{
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


//route to update a task "PUT"
//this rute expects to recive a PUT request at the URL "/tasks/:id"
router.put("/:id", (req, res)=>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

//activity
//1.Create a route for getting a specific task.
router.get("/:id", (req, res)=>{
	taskController.retrieveTask(req.params.id).then(resultFromController => res.send(resultFromController));
})
//5.Create a route for changing the status of a task to "complete".
router.put("/:id/complete",(req,res)=>{
	taskController.completeTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

//module,exports toe xport the router object to use in the index.js
module.exports = router;