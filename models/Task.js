//create the schema, model and export the file

const mongoose = require("mongoose");

//schema creation

const taskSchema = new mongoose.Schema({
	name:String,
	status:{
		type : String,
		default: "pending"
	}

});

//module.exports is a way for NodeJS to treat this value a package that can be used by other files
module.exports = mongoose.model("Task", taskSchema);