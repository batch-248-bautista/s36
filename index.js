//set updependencies
//import modules
const express = require("express");
const mongoose = require("mongoose");

//this allows us to use all the routes
const taskRoutes = require("./routes/taskRoutes")

//set up server
const app = express();
const port = 4000;

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//datbaseconnections
//connecting to mongodb atlas

mongoose.set('strictQuery', true);

mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.jeupjjp.mongodb.net/s35?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

//connection to databse
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"
	));
db.once("open",()=>console.log("Hi! We are connected to MongoDB Atlas!"))


//add the task route
//allows us the task routes creted in the taskROute.js file to use "/tasks" route

app.use("/tasks", taskRoutes)

//server listennig
app.listen(port,()=>console.log(`Now listening to port ${port}`))