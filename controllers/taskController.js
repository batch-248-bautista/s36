//controllers containthe fuctions and business logiv of our express JS application
//meaning all theopearations it can do willbe placedin this file

const Task = require("../models/Task");
//controller fucntion foe gettig  all the tasks
//define fucntions tobeused in the taskRoutes.js and export these function

module.exports.getAllTasks = () => {
	//the "return"satement, return the result of the Mongoose method "find"back to the TaskRoutes.js file
	return Task.find({}).then(result=>{
		return result;
	})
}

//Mini Activity Start
// Controller function for creating a task
// The request body coming from the client was passed from the "taskRoutes.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {

	// Creates a task object based on the Mongoose model "Task"
	let newTask = new Task({
		
		// Sets the "name" property with the value received from the client/Postman
		name : requestBody.name

	})

	// Saves the newly created "newTask" object in the MongoDB database
	// The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/Postman
	// The "then" method will accept the following 2 arguments:
		// The first parameter will store the result returned by the Mongoose "save" method
		// The second parameter will store the "error" object
	// Compared to using a callback function on Mongoose methods discussed in the previous session, the first parameter stores the result and the error is stored in the second parameter which is the other way around
		// Ex.
			// newUser.save((saveErr, savedTask) => {})
	return newTask.save().then((task, error) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if (error) {

			console.log(error);
			// If an error is encountered, the "return" statement will prevent any other line or code below it and within the same code block from executing
			// Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code
			// The else statement will no longer be evaluated
			return false;

		// Save successful, returns the new task object back to the client/Postman
		} else {

			return task; 

		}

	})

}

//Mini Activity End

//businesslogic
// controller fucntion foe delteing a task
// "taskId" is the URL paramter passed frim the taskRutes.js
/*
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using Mongoose MEthod "findBYIDAndRemove" with the same id peocided in the route
*/

module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask, error)=>{
		if(error){
			console.log(error);
			return false
		}
		else{
			return `${removedTask.name} has been deleted.`;
		}
	})
}

//controller function for updatng a task
// business logic
/*
1. Get the task with the Id using the Mongoose method "findById" 
2. replace the task's ame returnes form the db with the "name" property form the request body
3. save the task

*/

module.exports.updateTask = (taskId, newContent)=>{
	return Task.findById(taskId).then((result, error)=> {
		if(error){
			console.log(error);
			return false
		}
		result.name = newContent.name

		return result.save().then((updatedTask, saveErr)=>{
			if (saveErr){
				console.log(saveErr);
				return false
			}
			else{
				return updatedTask;
			}
		})
	})
}

//2.Create a controller function for retrieving a specific task.
module.exports.retrieveTask = (taskId, retrievedTask) =>{
	return Task.findById(taskId).then((result, error)=>{
		if(error){
			console.log(error);
			return false
		}
	
		else{
			return result;
		}
		
	})
}

//6.
module.exports.completeTask = (taskId, newStatus)=>{
	return Task.findById(taskId).then((result, error)=> {
		if(error){
			console.log(error);
			return false
		}
		result.status = newStatus.status

		return result.save().then((updatedStatus, saveErr)=>{
			if (saveErr){
				console.log(saveErr);
				return false
			}
			else{
				return updatedStatus;
			}
		})
	})
}

